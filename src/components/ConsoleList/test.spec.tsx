/*-----------------------------------------------------------------------------
 * @package:    ConsoleList
 * @author:     Richard B Winters
 * @copyright:  2018 Massively Modified, Inc.
 * @license:    Apache-2.0 <http://www.apache.org/licenses/LICENSE-2.0>
 * @version:    0.1.0
 *---------------------------------------------------------------------------*/


// INCLUDES
import * as React from 'react';
import * as ShallowRenderer from 'react-test-renderer/shallow';

import { ConsoleListProps, ConsoleList } from './';


// Basic ConsoleList Test:
it
(
    'ConsoleList renders correctly',
    () =>
    {
        let componentProperties: ConsoleListProps =
        {
            consoleItemList: [
                "Test line 1",
                "Test line 2",
                "Test line 3"
            ]
        };

        let renderer = ShallowRenderer.createRenderer();

        renderer.render
        (
            <ConsoleList
              consoleItemList={componentProperties.consoleItemList}
            />
        );

        expect( renderer.getRenderOutput() ).toMatchSnapshot();
    }
);

