/*------------------------------------------------------------------------------
 * @package:   kwaeri-core-react
 * @author:    Richard B Winters
 * @copyright: 2018 Richard B Winters.
 * @license:   Apache v2.0
 * @version:   1.0.0
 *----------------------------------------------------------------------------*/


// INCLUDES
import {
    FETCH_USERS_BEGIN,
    FETCH_USERS_SUCCESS,
    FETCH_USERS_ERROR
} from '../constants/action-types';


// DEFINES
const initialState = {
    loading: false,
    error: <any>null,
    users: {
        invalidate: true,
        lastUpdated: <any>null,
        list: <any>[]
    }
};


// Create a reducer:
const users = ( state = initialState, action: any ) =>
{
    console.log( 'Action.type = ' )
    console.log( action.type );
    switch( action.type )
    {
        case FETCH_USERS_BEGIN:
        {
            console.log( 'Reducer fetch-users-begin triggered' );
            // The loading state. Render a loader and wipe errors:
            return { ...state, loading: true, error: null };
        }break;

        case FETCH_USERS_SUCCESS:
        {
            console.log( 'Reducer fetch-users-success triggered' );
            console.log( 'Payload:' );
            console.log( action.payload.users );
            // The success state. We have the API response, render it:.
            return { ...state, loading: false, users: { list: action.payload.users , invalidate: false, lastUpdated: Date.now() } };
        }break;

        case FETCH_USERS_ERROR:
        {
            console.log( 'Reducer fetch-users-error triggered' );
            // The error state. Render the error in our simple template app:
            return { ...state, loading: false, error: action.payload.error, users: { list: [], invalidate: false, lastUpdated: null } };
        }break;

        default:
        {
            console.log( 'default reducer action case triggered');
            return state;
        }break;
    }
};


// Export it:
export default users;
