import * as React from 'react';
export interface KwaeriCoreProps {
    title?: string;
}
/**
 * KwaeriCore component
 *
 * @param { KwaeriCoreProps } props
 *
 * @since 0.1.0
 */
export declare const KwaeriCore: React.SFC<KwaeriCoreProps>;
/**
 * We'd set the propTypes here if this wasn't Typescript.
 * We'd set the defaultProps here if we wanted to.
 */
