/****************************************************************************\
 * @package:    Console
 * @author:     Richard B Winters
 * @copyright:  2018 Massively Modified, Inc.
 * @license:    Apache-2.0 <http://www.apache.org/licenses/LICENSE-2.0>
 * @version:    0.1.0
 *---------------------------------------------------------------------------*/


// INCLUDES
import * as React from 'react';
import * as ShallowRenderer from 'react-test-renderer/shallow';

import { ConsoleProps, Console } from './';
import { ConsoleLegendItemProps, ConsoleLegendItem } from '../ConsoleLegendItem';


// Basic Console Test:
it
(
    'Console renders correctly',
    () =>
    {
        let componentProperties: ConsoleProps =
        {
            consoleInput: "ls ..",
            consoleOutput: ["<span className=\"file-item\">lib</span>"],
            handleOnChangeForInput: () => {},
            handleSubmit: () => {},
            legendData: [
                <ConsoleLegendItem
                    legendKey="cd"
                    legendDescData="Changes the current directory."
                    legendUsageData="cd <path>"
                    handleMouseOver={() => {}}
                    handleMouseOut={() => {}}
                />
            ]
        };

        let renderer = ShallowRenderer.createRenderer();

        renderer.render
        (
            <Console
              consoleOutput={componentProperties.consoleOutput}
              consoleInput={componentProperties.consoleInput}
              handleOnChangeForInput={componentProperties.handleOnChangeForInput}
              handleSubmit={componentProperties.handleSubmit}
              legendData={componentProperties.legendData}
            />
        );

        expect( renderer.getRenderOutput() ).toMatchSnapshot();
    }
);

