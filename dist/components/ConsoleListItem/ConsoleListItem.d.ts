import * as React from 'react';
export declare type ConsoleListItemProps = {
    content: string;
};
/**
 * ConsoleListItem presentational component
 *
 * @param { ConsoleListItemProps } props
 *
 * @since 0.1.0
 */
export declare const ConsoleListItem: React.SFC<ConsoleListItemProps>;
/**
 * We'd set the propTypes here if this wasn't Typescript.
 * We'd set the defaultProps here if we wanted them.
 */
