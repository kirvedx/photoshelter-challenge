declare const store: import("redux").Store<{
    users: {
        loading: boolean;
        error: any;
        users: {
            invalidate: boolean;
            lastUpdated: any;
            list: any;
        };
    };
    consoleItems: {
        consoleItems: any;
    };
}, import("redux").AnyAction> & {
    dispatch: {};
};
export default store;
