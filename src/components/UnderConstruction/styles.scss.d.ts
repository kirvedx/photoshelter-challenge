export const underConstructionContainer: string;
export const focusHeader: string;
export const titleCapture: string;
export const focusContent: string;
export const ucWrap: string;
export const ucMessage: string;
export const ucContentContainer: string;
