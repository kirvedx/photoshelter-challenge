/*-----------------------------------------------------------------------------
 * @package:    ConsoleLegend
 * @author:     Richard B Winters
 * @copyright:  2018 Massively Modified, Inc.
 * @license:    Apache-2.0 <http://www.apache.org/licenses/LICENSE-2.0>
 * @version:    0.1.0
 *---------------------------------------------------------------------------*/


// INCLUDES
import * as React from 'react';
import * as ShallowRenderer from 'react-test-renderer/shallow';

import { ConsoleLegendProps, ConsoleLegend } from './';
import { ConsoleLegendItemProps, ConsoleLegendItem } from '../ConsoleLegendItem';


// Basic ConsoleLegend Test:
it
(
    'ConsoleLegend renders correctly',
    () =>
    {
        let componentProperties: ConsoleLegendProps =
        {
            content: [
                <ConsoleLegendItem
                    legendKey="cd"
                    legendDescData="Changes the current directory."
                    legendUsageData="cd <path>"
                    handleMouseOver={() => {}}
                    handleMouseOut={() => {}}
                />
            ]
        };

        let renderer = ShallowRenderer.createRenderer();

        renderer.render
        (
            <ConsoleLegend
              content={componentProperties.content}
            />
        );

        expect( renderer.getRenderOutput() ).toMatchSnapshot();
    }
);

