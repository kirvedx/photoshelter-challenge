import * as React from 'react';
export interface AppProps {
    title?: string;
}
/**
* App container component
*
* @since 0.1.0
*/
export declare class App extends React.Component<AppProps, any> {
    /**
     * @vqar { string } displayName Always set the display name
     *
     * @since 0.1.0
     */
    displayName: string;
    /**
     * We'd set propTypes here if this wasn't Typescript.
     */
    /**
     * @var { any } defaultProps The App's default property values
     *
     * @since 0.1.0
     */
    defaultProps: {
        title: string;
    };
    /**
     * Constructor
     *
     * @param props
     *
     * @since 0.1.0
     */
    constructor(props?: AppProps);
    /**
     * Container components pass rendering responsibilities off to
     * presentational components
     *
     * @param { void }
     *
     * @since 0.1.0
     */
    render(): JSX.Element;
}
declare const _default: import("react-redux").ConnectedComponentClass<typeof App, Pick<AppProps, "title">>;
export default _default;
