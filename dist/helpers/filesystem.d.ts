export declare type ParsedInputType = {
    command: boolean | string;
    arguments: boolean | Array<string>;
    error?: string;
};
export declare class FilesystemHelper {
    commands: {
        cd: {
            desc: string;
            usage: string;
        };
        pwd: {
            desc: string;
            usage: string;
        };
        mkdir: {
            desc: string;
            usage: string;
        };
        rmdir: {
            desc: string;
            usage: string;
        };
        symlink: {
            desc: string;
            usage: string;
        };
        touch: {
            desc: string;
            usage: string;
        };
        ls: {
            desc: string;
            usage: string;
        };
    };
    /**
     * Class constructor
     *
     * @since 0.1.0
     */
    constructor();
    /**
     * Method which parses input in order to determine a command and provided arguments
     *
     * @param { string } input The string input provided by the user of a virtual terminal
     *
     * @return { ParsedInputType } An object which specifies the found command and arguments, or an error otherwise
     *
     * @since 1.0.0
     */
    parseInput(input: string): ParsedInputType;
    /**
     * Runs a routine which is described in exercise 1 of the Photoshelter challenge
     *
     * @return { Array<string> } Returns an array of strings to be potentially displayed in a virtual console/terminal.
     */
    runExercise1(): string;
}
