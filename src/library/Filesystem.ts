/*-----------------------------------------------------------------------------
 * @package:    kwaeri-core-react
 * @author:     Richard B Winters
 * @copyright:  2018 Massively Modified, Inc.
 * @license:    Apache-2.0 <http://www.apache.org/licenses/LICENSE-2.0>
 * @version:    1.0.0
 *---------------------------------------------------------------------------*/


// INCLUDES


// DEFINES
export type FileSystemMetaType = {
    type: string;
    link?: string|null;
};

export type FileSystemType = {
    [key: string]: FileSystemMetaType;
}

export type FileSystemErrorType = {
    error: string;
    message: string;
}


export class Filesystem
{
    /**
     * @var { string } currentDirectory Stores the current working directory.
     */
    currentDirectory: string = "/";


    /**
     * @var { FileSystemType } filesystem The current representation of a virtual filesystem
     */
    filesystem: FileSystemType = {};


    /**
     * Class constructor.
     *
     * @since 1.0.0
     */
    constructor( cwd?: string, filesystem?: FileSystemType )
    {
        // Set the filesystem if it is provided:
        if( filesystem )
        {
            this.filesystem = filesystem;
        }
        else
        {
            // Initialize a filesystem with at least a root directory
            // otherwise:
            this.filesystem['/'] = { type: "directory" };
        }

        // Set the current working directory if it is provided and it exists:
        if( cwd && cwd !== "" )
        {
            this.currentDirectory = cwd;
        }
    }


    /**
     * Method which checks if a path identifier is valid
     *
     * @param { string } path An absolute or relative path string
     *
     * @return { boolean } True if a path string is valid, false if not
     */
    isValidIdentifier( path: string ): boolean
    {
        return ( path && typeof path === "string" && /^[a-zA-Z\/.]+$/.test( path ) );
    }


    /**
     * Method that checks if a path is absolute or relative, and subsequently
     * returns an absolute path
     *
     * @param { string } path An absolute or relative path
     *
     * @return { string } An absolute path
     *
     * @since 1.0.0
     */
    getFullPath( path: string ): string
    {
        // Figure out if the path is absolute or relative:
        let absolute = ( ( path.charAt( 0 ) == "/" ) ? true : false ),
            virtual = "",
            parts = [];

        // If the path possessess some ".." characters, we need to go up some level(s):
        if( path.includes( ".." ) )
        {
            // We'll convert the provided path to parts
            let parts = ( absolute ) ? ( path.substr( 1, path.length ) ).split( "/" ) : path.split( "/" ),
                currentPath = this.currentDirectory;

            // Cover some bases which may have been overlooked:
            if( !parts && path == ".." )
            {
                parts = [".."];
            }

            // For each part of the provided path:
            for( let paths in parts )
            {
                // If the part is a level up operator:
                if( parts[paths] === ".." )
                {
                    // Then just take a slice off the end of the current directory:
                    currentPath = this.getParentDirectory( currentPath );
                }
                else
                {
                    // Otherwise, tac on the directory that is indicated:
                    if( currentPath == "/" )
                    {
                        currentPath += parts[paths];
                    }
                    else
                    {
                        currentPath += "/" + parts[paths];
                    }
                }
            }

            return currentPath;
        }

        // Then return either the path appended to the current directory,
        // or the path alone - depending on whether it was an absolute
        // path or not, respectively:
        return ( ( absolute ) ? path : ( this.currentDirectory == "/" ) ? this.currentDirectory + path : this.currentDirectory + '/' + path );
    }


    /**
     * A method which checks to see that a path exists within the filesystem. It will call
     * checkPathExistsViaSymlink first, then check if the path exists directly..
     *
     * @param { string } path The absolute path to the directory which needs to be checked for existence
     *
     * @return { number|boolean } 1 if the path exists directly, 2 if the path exists via symlink, or false if not. .
     */
    checkPathExists( path: string ): number|boolean
    {
        // Check if the path is valid:
        if( !this.isValidIdentifier( path ) )
        {
            return false;
        }

        // Check whether the path is to the CWD, to a directory which currently exists, or a symlinked path which currently exists:
        return (
            ( this.checkPathExistsViaSymlink( path ) ) ? this.checkPathExistsViaSymlink( path ) : ( path == "./" || this.filesystem.hasOwnProperty( path ) ) ? 1 : false
        );
    }


    /**
     * A method which checks to see that a target path exists within the filesystem through the use of symlinks
     *
     * @param { string } path The absolute path to the directory which needs to be checked for existence
     *
     * @return { number|boolean } 2 if the path exists via symlink, false if not.
     */
    checkPathExistsViaSymlink( path: string ): number|boolean
    {
        // Do a check on our symlink
        let symlink = this.findSymlinkInPath( path );

        // If no symlink was found, or the identifier is invalid - return false:
        if( !symlink )
        {
            return false;
        }

        // Otherwise see if the path following what was found as a symlink
        // exists as a child path of the linked directory that the symlink
        // points to:
        let count       = ( symlink as string ).length,
            remainder   = path.substr( count, path.length );

        // Tac the remainder onto the link pointed to by the symlink -
        // and see if that directory exists as already mentioned:
        return ( ( this.filesystem[this.filesystem[symlink as string].link + remainder] ) ? 2 : false );
    }


    /**
     * A method which finds a symlink within a path chain, if one exists
     *
     * @param { string } path The aboslute path to a target which possibly possess a symlink reference along the chain
     *
     * @return { string|boolean } If the path is found to contain a symlink, the symlink portion is returned - otherwise false is returned.
     *
     * @since 1.0.0
     */
    findSymlinkInPath( path: string ): boolean|string
    {
        // Check whether the full path IS a symlink. If so, return it:
        if( this.filesystem.hasOwnProperty( path ) &&
            this.filesystem[path].type == "directory" &&
            this.filesystem[path].link )
        {
            return path;
        }

        // If the full path wasnt a symlink, we need to recursively check
        // whether each parent directory is a symlink:
        let currentParent = path,
            continueSearch = true;

        while( currentParent !== "/" && continueSearch )
        {
            currentParent = this.getParentDirectory( currentParent );

            if( this.filesystem.hasOwnProperty( currentParent ) &&
                this.filesystem[currentParent].type == "directory" &&
                this.filesystem[currentParent].link )
            {
                // We found the symlink:
                continueSearch = false;
            }
        }

        // If we did find the symlink:
        if( !continueSearch )
        {
            return currentParent;
        }

        // Otherwise report that a symlink did not exist:
        return false;
    }


    /**
     * Method which builds a real path by replacing a symlink portion of the path chain.
     *
     * @param { string } path A relative or absolute path to a target
     *
     * @return { string } The original path with a symlink portion of its chain replaced by the symlink's link, or REAL, path.
     *
     * @since 1.0.0
     */
    replaceSymlinkInPath( path: string ): string
    {
        // It exists via a symlink, let's get the symlink piece:
        let symlink     = this.findSymlinkInPath( this.getFullPath( path ) );

        // Let's get the remaining piece of the path chain from
        // after the symlink:
        let count       = ( symlink as string ).length,
        remainder   = this.getFullPath( path ).substr( count, this.getFullPath( path ).length );

        // Glue the pieces together to create the REAL path:
        return this.filesystem[symlink as string].link + remainder;
    }


    /**
     * A method which checks and returns the full/absolute REAL path of a target.
     *
     * @param { string } path The absolute or relative path to the directory.
     *
     * @return { string|boolean } If the path exists,the full/absolute REAL path string is returned, false if not.
     */
    getRealPath( path: string ): string|false
    {
        // Check to ensure the path does exist:
        let existsAs = this.checkPathExists( this.getFullPath( path ) );

        switch( existsAs )
        {
            case 1:
            {
                // It exists directly within our filesystem:
                return this.getFullPath( path );
            }break;

            case 2:
            {
                // It exists through a symlink:
                return this.replaceSymlinkInPath( path );
            }break;

            default:
            {
                // The path does not exist:
                return false;
            }break;
        }
    }


    /**
     * Method that returns the absolute path to the parent directory of a directory or file that is  passed in as an absolute path
     *
     * @param { string } path The absolute path to a file or directory
     *
     * @return { string } The aboslute path to the parent directory of the path that's provided
     */
    getParentDirectory( path: string )
    {
        // If path is root, or empty - return the root path:
        if( path == "/" || path == "" )
        {
            return path = "/";
        }

        // Otherwise return the path string preceeding the final path segment:
        return ( ( path.substr( 1, path.length ) ).includes( '/' ) ) ? "/" + ( ( ( path.substr( 1, path.length ) ).split( '/' ) ).slice( 0, -1 ).join( '/' ) ) :  "/";
    }


    /**
     * Method which checks for children within a directory.
     *
     * @param { string } path The absolute path to a directory.
     * @param { boolean } check A boolean for whether to simply report a child is present, or to return a list of children.
     *
     * @return { boolean|Array<string> } A boolean for whether children exist or not, otherwise an array of children is returned if found.
     *
     * @since 1.0.0
     */
    getChildren( path: string, check?: boolean ): boolean|Array<string>
    {
        // If the path provided is a valid identifier:
        if( this.checkPathExists( this.getFullPath( path ) ) )
        {
            // Prepare some variables for control:
            let listing = Object.keys( this.filesystem ),
                fullPath = this.getRealPath( path ) as string,
                children = [];

            // Loop through each file within the filesystem listing:
            for( let files in listing )
            {
                // If any listing includes the provided path as part of its identifier:
                if( listing[files].includes( fullPath ) && listing[files] !== fullPath )
                {
                    // If this is simply a check:
                    if( check )
                    {
                        // Return the instant we find that a child exists:
                        return true;
                    }

                    // Otherwise compile a list of children to be returned:
                    children.push( listing[files] );
                }
            }

            // If it was just a check, return false indicating that no children were found:
            if( check )
            {
                return false;
            }

            // Otherwise, return the list of found children:
            return children;
        }

        return false;
    }


    /**
     * Method which checks for direct children of a directory.
     *
     * @param { string } path The absolute path to a directory.
     *
     * @return { boolean|Array<any> } An array of children is returned if found, otherwise false is returned.
     *
     * @since 1.0.0
     */
    getDirectChildren( path: string ): boolean|any
    {
        // If the path provided is a valid identifier:
        if( this.checkPathExists( this.getFullPath( path ) ) && this.filesystem[this.getRealPath( path ) as string].type === "directory" )
        {
            // Prepare some variables for control:
            let listing = Object.keys( this.filesystem ),
                // Ensure that if the chosen path to list children for is a symlink, that we list children from it's link:
                fullPath = this.getRealPath( path ) as string,
                children: any = {};

            // Loop through each file within the filesystem listing:
            for( let files in listing )
            {
                // This regex matches everything between /current/path/ and a possible trailing "/child's/children/":
                let regex = ( fullPath === "/" ) ? new RegExp( "^\\/([a-zA-Z]+)\\/?$" ) : new RegExp( "^" + fullPath + "\\/([a-zA-Z]+)\\/?$" );

                // If the current listing is not that of the target we're finding children for:
                if( listing[files] !== fullPath )
                {
                    // Execute our regex on the listing:
                    let matched = regex.exec( listing[files] );

                    // Assign the first capture group to found:
                    let found = ( matched && matched.length > 1 ) ? matched[1] : false;

                    // If found is assigned a value:
                    if( found )
                    {
                        // Populate our children array
                        children[found] = { type: this.filesystem[listing[files]].type, link: ( this.filesystem[listing[files]].link ) ? true : false };
                    }
                }
            }

            // Return the list of found children:
            return children;
        }

        // Otherwise return false
        return false;
    }


    /**
     * Method to change directories.
     *
     * @param { string } path A relative or absolute path to another directory. Can use `..` to signify the current directory's parent directory.
     *
     * @return { boolean|FileSystemErrorType } True if the directory was changed, the error for why-not if it was not.
     *
     * @since 1.0.0
     */
    cd( path: string ): boolean|FileSystemErrorType
    {
        // Sometimes people type too quickly, or simply forget:
        if( !path || path === "" )
        {
            return { error: `Could not change the current directory.`, message: `No target directory was supplied.` };
        }

        // Let's check if the REAL path exists:
        if( this.checkPathExists( this.getFullPath( path ) ) )
        {
            // Set the current directory to the absolute path requested as long
            // as it is a diretory type path (symlinks will inherit their linked
            // path types):
            if( this.filesystem[this.getRealPath(path ) as string].type === "directory" )
            {
                this.currentDirectory = this.getFullPath( path );

                // And flag that the directory change occurred:
                return true;
            }

            // Return the result for the attempt to change the current working directory:
            return { error: `Could not change the current directory to '${this.getFullPath( path )}'.`, message: `The target path '${this.getFullPath( path )}' is not a directory.` };
        }

        // Return the result for the attempt to change the current working directory:
        return { error: `Could not change the current directory to '${this.getFullPath( path )}'.`, message: `The target directory '${this.getFullPath( path )}' does not exist.` };
    }


    /**
     * Method to print the path to the current working directory.
     *
     * @param { void }
     *
     * @return { string } The absolute path to the current working directory.
     *
     * @since 1.0.0
     */
    pwd(): string
    {
        // Simply print the current working directory's absolute path:
        return this.currentDirectory;
    }


    /**
     * A method to add a new directory to the filesystem.
     *
     * @param { string } path The absolute or relative path to the new directory to create.
     *
     * @return { boolean|FileSystemErrorType } True; signifying success, or an error message explaining why the new directory could not be made.
     *
     * @since 1.0.0
     */
    mkdir( path: string ): boolean|FileSystemErrorType
    {
        // Sometimes people type too quickly, or simply forget:
        if( !path || path === "" )
        {
            return { error: `Could not create the target.`, message: `No target was supplied.` };
        }

        // If the identifier is valid and the path doesnt already exist:
        if( this.isValidIdentifier( path ) && !this.checkPathExists( this.getFullPath( path ) ) )
        {
            // Check that the absolute path to the parent directory of the
            // provided path exists:
            if( this.checkPathExists( this.getParentDirectory( this.getFullPath( path ) ) ) )
            {
                // And if so, create the new path by adding it to the
                // filesystem if the path is that of a directory type:
                if( this.filesystem[this.getRealPath( this.getParentDirectory( this.getFullPath( path ) ) ) as string].type === "directory" )
                {
                    // Handle a case for a symlink being in the path:
                    if( this.checkPathExists( this.getParentDirectory( this.getFullPath( path ) ) ) == 2 )
                    {
                        // Replace the symlink in the path to create the new directory
                        // at its REAL location:
                        this.filesystem[this.replaceSymlinkInPath( path )] = { type: "directory" };

                        return true;
                    }

                    // If the parent has no symlink, its safe to use the absolute full path
                    // with out calling getRealPath():
                    this.filesystem[this.getFullPath( path )] = { type: "directory" };

                    // And return an indicator that the operation was successful:
                    return true;
                }

                // Return the result for the attempt to change the current working directory:
                return { error: `Could not create '${this.getFullPath( path )}'.`, message: `The target destination '${this.getParentDirectory( this.getFullPath( path ) )}' is not a directory.` };
            }

            // Otherwise return an error as the parent directory did not exist,
            // which does not allow for us to create a directory within it:
            return { error: `Could not create '${this.getFullPath( path )}'.`, message: `The target destination directory '${this.getParentDirectory( this.getFullPath( path ) )}' does not exist.` };
        }

        // Otherwise, return an error as the identifier was not valid or the
        // directory already exists:
        return { error: `Could not create '${this.getFullPath( path )}'.`, message: `The target directory '${this.getFullPath( path )}' already exists.` };
    }


    /**
     * A method to remove a directory from the filesystem.
     *
     * @param { string } path The absolute or relative path to the directory to remove.
     *
     * @return { boolean|FileSystemErrorType } True; signifying success, or an error message explaining why the directory could not be removed.
     *
     * @since 1.0.0
     */
    rmdir( path: string ): boolean|FileSystemErrorType
    {
        // Sometimes people type too quickly, or simply forget:
        if( !path || path === "" )
        {
            return { error: `Could not remove the target.`, message: `No target was supplied.` };
        }

        // If the identifier is valid and the path exists:
        if( this.checkPathExists( this.getFullPath( path ) ) )
        {
            // Check if there are any children: (the getChildren method will
            // handle getting the absolute path)
            if( !this.getChildren( path, true ) )
            {
                // As long as there weren't any children the path may be
                // deleted:
                delete this.filesystem[this.getRealPath( path ) as string];

                // And we can indicate that this was the case:
                return true;
            }

            // Otherwise, retun false to indicate that children existed within the target directory;
            return { error: `Could not delete '${this.getFullPath( path )}'.`, message: `The target directory '${this.getFullPath( path )}' contains files and/or other directories..` };
        }

        // Otherwise, retun false to indicate that the target directory did not exist;
        return { error: `Could not delete '${this.getFullPath( path )}'.`, message: `The target directory '${this.getFullPath( path )}' does not exist.` };
    }


    /**
     * A method to register a symlink within the filesystem.
     *
     * @param { string } source The absolute or relative path to the directory or file within the filesystem that the symlink being created should point to.
     * @param { string } dest The absolute or relative path to where a symlink directory or file should be created within the filesystem.
     *
     * @return { boolean|FileSystemErrorType } True; signifying success, or an error message explaining why the symlink could not be created.
     *
     * @since 1.0.0
     */
    symlink( source: string, dest: string ): boolean|FileSystemErrorType
    {
        // Sometimes people type too quickly, or simply forget:
        if( !source || source === "" || !dest || dest === "" )
        {
            return { error: `Could not create the symlink.`, message: `A valid source and destination must be supplied.` };
        }

        // If the source identifier is valid and the path exists:
        if( this.checkPathExists( this.getFullPath( source ) ) )
        {
            // If the dest identifier is valid and the destination path doesn't already exist:
            if( this.isValidIdentifier( dest ) && !this.checkPathExists( this.getFullPath( dest ) ) )
            {
                // Check that the absolute path to the parent directory of the
                // provided destination path exists:
                if( this.checkPathExists( this.getParentDirectory( this.getFullPath( dest ) ) ) )
                {
                    // Check that the REAL path to the parent directory of the indicated
                    // destination is a directory:
                    if( this.filesystem[this.getRealPath( this.getParentDirectory( this.getFullPath( dest ) ) ) as string].type === "directory" )
                    {
                        // And if so, create the symlink by adding it to the
                        // filesystem much like we do with a directory or file -
                        // keeping in mind the type that the link should refer to:
                        let type = this.filesystem[this.getRealPath( source ) as string].type;

                        // Handle a case for a symlink being in the path:
                        if( this.checkPathExists( this.getParentDirectory( this.getFullPath( dest ) ) ) == 2 )
                        {
                            // Replace the symlink in the path to create the symlink
                            // at its REAL location:
                            this.filesystem[this.replaceSymlinkInPath( dest )] = { type: type, link: this.getRealPath( source ) as string };

                            return true;
                        }

                        // Otherwise it is safe to use the absolute path
                        // to create the symlink:
                        this.filesystem[this.getFullPath( dest )] = { type: type, link: this.getRealPath( source ) as string };

                        // And return an indicator that the operation was successful:
                        return true;
                    }

                    // Return the result for the attempt to change the current working directory:
                    return { error: `Could not create the symlink '${this.getFullPath( dest )}'.`, message: `The destination '${this.getParentDirectory( this.getFullPath( dest ) )}' is not a directory.` };
                }

                // Otherwise return the result as the parent directory did not exist,
                // which does not allow for us to create a directory within it:
                return { error: `Could not create the symlink '${this.getFullPath( dest )}'.`, message: `The destination directory for the target '${this.getFullPath( dest )}' does not exist.` };
            }

            // Otherwise, return the result as the identifier was not valid or the
            // target symlink already exists:
            return { error: `Could not create the symlink '${this.getFullPath( dest )}'.`, message: `The target '${this.getFullPath( dest )}' already exists.` };
        }

        // Otherwise, return the result as the identifier was not valid or the
        // source directory doesn't exists:
        return { error: `Could not create the symlink '${this.getFullPath( dest )}'.`, message: `The source '${this.getFullPath( dest )}' does not exist.` };
    }


    /**
     * A method to create a new file within the filesystem
     *
     * @param { string } path The absolute or relative path leading to and including the file name, where a file should be created within the filesystem.
     *
     * @return { boolean|FileSystemErrorType } True; signifying success, or an error message explaining why the file could not be created.
     *
     * @since 1.0.0
     */
    touch( path: string ): boolean|FileSystemErrorType
    {
        // Sometimes people type too quickly, or simply forget:
        if( !path || path === "" )
        {
            return { error: `Could not create the target.`, message: `No target was supplied.` };
        }

        // If the identifier is valid and the path doesnt already exist:
        if( this.isValidIdentifier( path ) && !this.checkPathExists( this.getFullPath( path ) ) )
        {
            // Check that the absolute path to the parent directory of the
            // provided path exists:
            if( this.checkPathExists( this.getParentDirectory( this.getFullPath( path ) ) ) )
            {
                // Check that the REAL path to the parent directory for the target path
                // is that of type 'directory':
                if( this.filesystem[this.getRealPath( this.getParentDirectory( this.getFullPath( path ) ) ) as string].type === "directory" )
                {
                    // Handle a case for a symlink being in the path:
                    if( this.checkPathExists( this.getParentDirectory( this.getFullPath( path ) ) ) == 2 )
                    {
                        // Replace the symlink in the path to create the new directory
                        // at its REAL location:
                        this.filesystem[this.replaceSymlinkInPath( path )] = { type: "file" };

                        return true;
                    }

                    // Otherwise it is safe to use the absolute path
                    // to create the file:
                    this.filesystem[this.getFullPath( path )] = { type: "file" };

                    // And return an indicator that the operation was successful:
                    return true;
                }

                // Return the result for the attempt to change the current working directory:
                return { error: `Could not create '${this.getFullPath( path )}'.`, message: `The target destination '${ this.getParentDirectory( this.getFullPath( path ) )}' is not a directory.` };
            }

            // Otherwise return the result as the parent directory did not exist,
            // which does not allow for us to create a directory within it:
            return { error: `Could not create '${this.getFullPath( path )}'.`, message: `The target destination directory '${this.getParentDirectory( this.getFullPath( path ) )}' does not exist.` };
        }

        // Otherwise, return the result as the identifier was not valid or the
        // directory already exists:
        return { error: `Could not create '${this.getFullPath( path )}'.`, message: `The target file '${this.getRealPath( path )}' already exists.` };
    }
}