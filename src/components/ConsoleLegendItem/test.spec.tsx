/*-----------------------------------------------------------------------------
 * @package:    ConsoleLegendItem
 * @author:     Richard B Winters
 * @copyright:  2018 Massively Modified, Inc.
 * @license:    Apache-2.0 <http://www.apache.org/licenses/LICENSE-2.0>
 * @version:    0.1.0
 *---------------------------------------------------------------------------*/


// INCLUDES
import * as React from 'react';
import * as ShallowRenderer from 'react-test-renderer/shallow';

import { ConsoleLegendItemProps, ConsoleLegendItem } from './';


// Basic ConsoleLegendItem Test:
it
(
    'ConsoleLegendItem renders correctly',
    () =>
    {
        let componentProperties: ConsoleLegendItemProps =
        {
            legendKey: "cd",
            legendDescData: "Changes the current directory.",
            legendUsageData: "cd <path>",
            handleMouseOver: () => {},
            handleMouseOut: () => {}
        };

        let renderer = ShallowRenderer.createRenderer();

        renderer.render
        (
            <ConsoleLegendItem
              legendKey={componentProperties.legendKey}
              legendDescData={componentProperties.legendDescData}
              legendUsageData={componentProperties.legendUsageData}
              handleMouseOver={componentProperties.handleMouseOver}
              handleMouseOut={componentProperties.handleMouseOut}
            />
        );

        expect( renderer.getRenderOutput() ).toMatchSnapshot();
    }
);

