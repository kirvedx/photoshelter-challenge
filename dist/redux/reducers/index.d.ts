declare const rootReducer: import("redux").Reducer<{
    users: {
        loading: boolean;
        error: any;
        users: {
            invalidate: boolean;
            lastUpdated: any;
            list: any;
        };
    };
    consoleItems: {
        consoleItems: any;
    };
}, import("redux").AnyAction>;
export default rootReducer;
