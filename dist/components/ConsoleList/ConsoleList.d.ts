import * as React from 'react';
import { ConsoleListItem } from '../ConsoleListItem';
export declare type ConsoleListProps = {
    consoleItemList: ConsoleListItem[];
};
/**
 * ConsoleList presentational component
 *
 * @param { ConsoleListProps } props
 *
 * @since 0.1.0
 */
export declare const ConsoleList: React.SFC<ConsoleListProps>;
/**
 * We'd set the propTypes here if this wasn't Typescript.
 * We'd set the defaultProps here if we wanted them.
 */
